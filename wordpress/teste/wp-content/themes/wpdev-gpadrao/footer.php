<?php // ?>

				<!-- Footer -->
					<footer id="footer">
						<div class="inner">

				
							<ul class="copyright">
								<li>&copy; Untitled. All rights reserved</li>
								<li>Tema WordPress desenvolvido por <a href="http://gpadrao.com.br">Gpadrao</a></li>
							</ul>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="<?php echo THEME_URL; ?>assets/js/jquery.min.js"></script>
			<script src="<?php echo THEME_URL; ?>assets/js/skel.min.js"></script>
			<script src="<?php echo THEME_URL; ?>assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="<?php echo THEME_URL; ?>assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="<?php echo THEME_URL; ?>assets/js/main.js"></script>
			<?php wp_footer(); ?>

	</body>
</html>