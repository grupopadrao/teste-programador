<?php

define( 'THEME_URL', get_bloginfo( 'template_url' ) . '/' );
define( 'SITE_NAME', get_bloginfo( 'name' ) );
define( 'SITE_URL',  get_bloginfo( 'url' ) );

add_action( 'after_setup_theme', 'custom_setup' );

function custom_setup()
{
	register_nav_menu( 'menu-header', 'Cabeçalho' );

	add_theme_support( 'post-thumbnails' );

	add_filter( 'excerpt_length',	'custom_excerpt_length' );
	add_filter( 'excerpt_more',		'custom_excerpt_more' );

	add_image_size( 'cover', 353, 326, true );

	add_filter( 'post_thumbnail_html', 'custom_thumbnail_html' );

	add_action( 'init', 'contact_sent' );
}

function custom_excerpt_length()
{
	return 10;
}

function custom_excerpt_more()
{
	return '...';
}

function custom_thumbnail_html( $html )
{
	return preg_replace(
        array(
            '/height="\d*"\s/',
            '/width="\d*"\s/'
        ),
        '',
        $html
    );
}

function contact_sent()
{
	global $contact_error;
	if ( isset( $_POST[ '_contact' ] ) ) {
		$values = array();
		$fields = array( '_name', '_email', '_message' );
		foreach ( $fields as $f ) {
			$v = sanitize_text_field( $_POST[ $f ] );
			if ( !$v ) {
				$contact_error = 'Preencha todos os campos...';
				break;
			}
			$values[ $f ] = $v;
		}

		if ( !$contact_error ) {
			$to = 'seuemail@email.com';
			$subject = 'Site - Formulário de contato';
			$message = sprintf(
				'Nome: %s' . PHP_EOL .
				'Email: %s' . PHP_EOL .
				'Mensagem: %s',
				$values[ '_name' ],
				$values[ '_email' ],
				$values[ '_message' ]
			);
			if ( !wp_mail( $to, $subject, $message ) )
				$contact_error = 'Não foi possível enviar a mensagem...';
			else
				wp_die( 'Mensagem enviada com sucesso!' );
		}
	}
}