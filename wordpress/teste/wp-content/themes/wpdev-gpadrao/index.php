<?php get_header(); ?>

<!-- Main -->
	<div id="main">
		<div class="inner">
			<header>
				<h1><?php bloginfo( 'description' ); ?></h1>
				<p>Etiam quis viverra lorem, in semper lorem. Sed nisl arcu euismod sit amet nisi euismod sed cursus arcu elementum ipsum arcu vivamus quis venenatis orci lorem ipsum et magna feugiat veroeros aliquam. Lorem ipsum dolor sit amet nullam dolore.</p>
			</header>


		<section class="tiles">
            <article class="style1">
                <span class="image">
                    <img src="<?php echo THEME_URL; ?>images/pic01.jpg" alt="" />
                </span>
                <a href="generic.html">
                    <h2>Magna</h2>
                    <div class="content">
                        <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                    </div>
                </a>
            </article>
            <article class="style2">
                <span class="image">
                    <img src="<?php echo THEME_URL; ?>images/pic02.jpg" alt="" />
                </span>
                <a href="generic.html">
                    <h2>Lorem</h2>
                    <div class="content">
                        <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                    </div>
                </a>
            </article>
            <article class="style3">
                <span class="image">
                    <img src="<?php echo THEME_URL; ?>images/pic03.jpg" alt="" />
                </span>
                <a href="generic.html">
                    <h2>Feugiat</h2>
                    <div class="content">
                        <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                    </div>
                </a>
            </article>
            <article class="style4">
                <span class="image">
                    <img src="<?php echo THEME_URL; ?>images/pic04.jpg" alt="" />
                </span>
                <a href="generic.html">
                    <h2>Tempus</h2>
                    <div class="content">
                        <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                    </div>
                </a>
            </article>
            <article class="style5">
                <span class="image">
                    <img src="<?php echo THEME_URL; ?>images/pic05.jpg" alt="" />
                </span>
                <a href="generic.html">
                    <h2>Aliquam</h2>
                    <div class="content">
                        <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                    </div>
                </a>
            </article>
            <article class="style6">
                <span class="image">
                    <img src="<?php echo THEME_URL; ?>images/pic06.jpg" alt="" />
                </span>
                <a href="generic.html">
                    <h2>Veroeros</h2>
                    <div class="content">
                        <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                    </div>
                </a>
            </article>


			</section>
  

			<nav class="pagination" style="margin-top: 2em;">

			</nav>

	
		</div>
	</div>

<?php get_footer(); ?>